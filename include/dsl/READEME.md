# 领域特定语言

domain-specific language, DSL

不重复自己的原则.

DSL 主要头注点是隐藏主代码的复杂性, 使主程序逻辑尽量简单, 
而牺牲大多数人看不见的底层部分.


