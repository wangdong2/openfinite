#ifndef GmshModel_h
#define GmshModel_h

#include <map>
#include <vector>
#include <math.h>
#include <iostream>
#include <gmsh.h>

namespace OF {
namespace GeometryModel {

template<class GK>
class GmshModel
{
public:
  typedef typename GK::Point_3 Point;
  typedef typename GK::Vector_3 Vector;
  typedef typename GK::Float F;
  typedef typename GK::Int I;

public:
  GmshModel(){}
                                           
  void project_to_face(const int fid, Point & p)
  {
    std::vector<double> point({p[0], p[1], p[2]});
    std::vector<double> close_p, close_pp;
    gmsh::model::getClosestPoint(2, fid, point, close_p, close_pp);
    p[0] = close_p[0];
    p[1] = close_p[1];
    p[2] = close_p[2];
  }

  void project_to_edge(const int eid, Point & p) 
  {
    std::vector<double> point({p[0], p[1], p[2]});
    std::vector<double> close_p, close_pp;
    gmsh::model::getClosestPoint(1, eid, point, close_p, close_pp);
    p[0] = close_p[0];
    p[1] = close_p[1];
    p[2] = close_p[2];
  }

  void project_vector_to_face(const int fid, const Point p, Vector & v)
  {
    std::vector<double> point({p[0], p[1], p[2]});
    std::vector<double> point_p, norm;
    gmsh::model::getParametrization(2, fid, point, point_p);

    // 获取法向
    gmsh::model::getNormal(fid, point_p, norm);
    double dot = v[0]*norm[0] +  v[1]*norm[1] +  v[2]*norm[2]; 
    v[0] -= dot*norm[0];
    v[1] -= dot*norm[1];
    v[2] -= dot*norm[2];
  }

  void project_vector_to_edge(const int eid, const Point p, Vector & v) 
  {
    std::vector<double> point({p[0], p[1], p[2]});
    std::vector<double> point_p, tangent;
    gmsh::model::getParametrization(1, eid, point, point_p);

    // 获取切向
    gmsh::model::getDerivative(1, eid, point_p, tangent);
    double dot = v[0]*tangent[0] +  v[1]*tangent[1] +  v[2]*tangent[2]; 
    v[0] = dot*tangent[0];
    v[1] = dot*tangent[1];
    v[2] = dot*tangent[2];
  }
};

}
}


#endif // end of GmshModel_h
